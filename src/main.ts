import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // @ts-ignore
  app.set('trust proxy', true)
  app.use((req, res, next) => {
    // const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddres;
    const ip = req.ip
    console.log('IP:', ip);

    next();
  })
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
