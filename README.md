You'll need to fork this repl: https://replit.com/@micalevisk/make-requests

```bash
npm ci

npm run start:dev

# in another terminal session
ngrok http 3000

# in another terminal session, run this 4 times in less than 1 minute
curl -I localhost:3000

# and go to repl.it to make another request from another IP
```
